<?php get_header(); ?>
  <div id="content">
  <?php get_sidebar(); ?>
    <div class="left_content">

	<?php if (have_posts()) : ?>

		<div class="alternative">Resultados de tu B&uacute;squeda</div>


		<?php while (have_posts()) : the_post(); ?>

			<div class="post" id="post-<?php the_ID(); ?>">

			<h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
              <div class="meta">
			     <span class="home_share">Comparte esta Nota!</span>
				 <ul class="share_ul">
				   <li><a href="http://twitter.com/share?url=<?php the_permalink(); ?>&amp;text=<?php the_title(); ?>&amp;count=horizontal&amp;lang=es" class="twitter-share-button">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script></li>
				   <li><iframe src="http://www.facebook.com/plugins/like.php?href=<?php the_permalink() ?>&amp;layout=button_count&amp;show_faces=true&amp;action=Me Gusta&amp;font=tahoma&amp;colorscheme=light&amp;width=130&amp;height=20" scrolling="no" frameborder="0" style="vertical-align:top;border:none; overflow:hidden; height:20px; width:130px; padding-top:1px;"></iframe></li>
				   <li><g:plusone size="medium" href="<?php the_permalink(); ?>"></g:plusone></li>
				 </ul>
				 <span class="home_coms"><?php comments_popup_link('0 Comentarios &#187;', '1 Comentario &#187;', '% Comentarios &#187;'); ?></span>
			  </div>

			<div class="entry">
				<?php the_content(); ?>
			</div>

			<div class="endpost">
				Guardado en: <?php the_category(', ') ?>
			</div>

			</div>

		<?php endwhile; ?>

		<?php include (TEMPLATEPATH . '/inc/nav.php' ); ?>

	<?php else : ?>

		<div class="alternative">No se encontraron Resultados, Intenta con otras Palabras</div>

	<?php endif; ?>
</div>
</div>
<?php get_footer(); ?>
